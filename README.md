# DCEL and Symmetric Data Structures

An implementation of the Doubly-Connected Edge List (DCEL) and Symmetric data structures for Plane Subdivisions (aka [Planar Straight-Line Graphs](https://en.wikipedia.org/wiki/Planar_straight-line_graph))

## Overview

The main command `src/plane_subdivision/run.py` lets users load a DCEL or Symmetric data structure stored in a data directory and query the data. Detail execution steps are generated as a query is executed and stored in directory `logs`.

## Setup

1. Python>=3.10 is required for running the code. Official distributions of Python can be found [here](https://www.python.org/downloads/).

2. Download the code in this repository and unzip it to a directory on your machine. We hrein refer to this directory as `base_directory`.

3. Open a command-line, terminal application such as Powershell on Windows, or Terminal on Mac/Linux systems. In the terminal, change the current directory to `base_directory` (see Step 2).

4. Verify that the setup is successful by running `python3 src/plane_subdivision/run.py --help`. **NOTE:** Depending on the Python installation on your machine, you may need to replace `python3` in the command by `py`. The command displays the help message of the main script. You know the setup is successful if the command prints a message similar to the one below.

```
usage: run.py [-h] --data_structure_name {DCEL,Symmetric} --data_directory DATA_DIRECTORY

options:
  -h, --help            show this help message and exit
  --data_structure_name {DCEL,Symmetric}
                        name of data structure
  --data_directory DATA_DIRECTORY
                        path to directory of data files to be loaded
```

## Usage

* Open a terminal window and change directory to `base_directory`.

* Supply the data structure name (i.e., `DCEL` or `Symmetric`) and the path to the directory that stores the data structure (e.g., `examples/dcel01` or `C:\Users\user\dcel01`) to the main command. The example script below loads DCEL data structure stored in `examples/dcel01` into the program. The data structure stored in the directory must match the data structure type specified.

**NOTE:** Depending on the Python installation on your machine, you may need to replace `python3` in the command by `py`.
```bash
python3 src/plane_subdivision/run.py --data_structure_name DCEL --data_directory examples/dcel01
```

* Once the data is successfully loaded, you see a confirmation and a propmt to submit a query as follow.

```
2024-02-17 10:58:50,438 INFO        run.py:41  - Data structure DCEL successfully loaded from examples/dcel01
Submit a query [e.g., EE(e1)] or type `q` to exit
>>>
```

* Follow the prompt to submit a query (e.g., type `FE(f1)` and hit Enter/Return). The program processes the query and prints the result on the screen and prompt for the next query.

```
>>> FE(f1)
>>> FE(f1) = ['e3', 'e4', 'e5', 'e6', 'e7', 'e1', 'e2']
Submit a query [e.g., EE(e1)] or type `q` to exit
>>>
```

* As the program executes a query, it logs the detail execution steps to a log file stored in `base_directory/logs`. [This](examples/20240217-105850.log) is  an example of such a log file. 

## Format of data files

### Data directory

The data structure of a plane subdivision is organised as a set of text files stored in a directory. For example, directory [examples/dcel01](examples/dcel01) stores 5 relations (EE, EF, EV, FE*, and VE*) of the plane subdivision according the the DCEL data structure. The file names indicate the relation types.

* [EE.txt](examples/dcel01/EE.txt) stores the EE relations of the plane subdivision,
* [EF.txt](examples/dcel01/EF.txt) stores the EF relations of the plane subdivision,
* [EV.txt](examples/dcel01/EV.txt) stores the EV relations of the plane subdivision,
* [FE_star.txt](examples/dcel01/FE_star.txt) stores the FE* relations of the plane subdivision,
* [VE_star.txt](examples/dcel01/VE_star.txt) stores the VE* relations of the plane subdivision.

### Data file

The text files encode data as comma-separated values. Each line represents a relation (e.g., a line in an EE.txt file stores all edges connected to a given edge). The first value in each line is the ID of the entity in question. The remaining values in the line compose the list of the IDs of entities which are associated with the first entity.

Symbol `#` indicates comments. All lines starting with `#` are ignored by the parser.

Below is the content of file EE.txt in example dcel01.

```
# edge-edge relations
e1,e7,e2
e2,e1,e12
e3,e2,e4
e4,e3,e5
e5,e4,e6
e6,e5,e7
e7,e6,e8
e8,e1,e9
e9,e8,e10
e10,e9,e11
e11,e10,e12
e12,e11,e3
```

* The first line of the file starting with `#` is a comment. 
* The second line `e1,e7,e2` specifies EE(e1) = [e7, e2]
* The third line `e2,e1,e12` specifies EE(e2) = [e1, e12]

## Example data structures provided with this program

Two example plane subdivisions are provided in directory [examples](examples). Each plane subdivision is represented in DCEL and Symmetric data structures. 

### Example 01

![Example 01](images/ex01.png)

* A DCEL representation of Example 01 is available in [examples/dcel01](examples/dcel01)
* A Symmetric representation of Example 01 is available in [examples/symmetric01](examples/symmetric01)

### Example 02

![Example 02](images/ex02.png)

* A DCEL representation of Example 02 is available in [examples/dcel01](examples/dcel02)
* A Symmetric representation of Example 02 is available in [examples/symmetric01](examples/symmetric02)