from config import DELIMITER, COMMENT


def parse(file):
    data = {}
    with open(file, 'r') as f:
        lines = f.readlines()
        for line in lines:
            if line.startswith(COMMENT):
                continue
            else:
                tokens = line.replace(' ', '').replace('\n', '').split(DELIMITER)
                key = tokens[0]
                value = tokens[1:]
                data[key] = value
    return data
