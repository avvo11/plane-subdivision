def remove_consecutive_duplicates(values: list) -> list:
    while __check_consecutive_duplicates(values):
        for idx in range(len(values) - 1):
            if values[idx] == values[idx + 1]:
                del values[idx + 1]
                break

    if values[len(values) - 1] == values[0]:
        del values[len(values) - 1]
    return values


def __check_consecutive_duplicates(values: list) -> bool:
    for idx in range(len(values) - 1):
        if values[idx] == values[idx + 1]:
            return True


def get_successor(entity, circular_list: list):
    """
    Returns the successor of the given `entity` in the `circular_list`.
    :param entity: given entity
    :param circular_list: circular list of entities
    :return: successor of `entity` in `circular_list`
    """
    for idx in range(len(circular_list)):
        if circular_list[idx] == entity:
            return circular_list[idx + 1] if idx != len(circular_list) - 1 else circular_list[0]
