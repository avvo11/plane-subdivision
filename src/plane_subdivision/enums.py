from enum import Enum


class DataStructureNames(Enum):
    DCEL = 'DCEL'
    Symmetric = 'Symmetric'


class NamedRelations(Enum):
    VV = 'VV'
    VE = 'VE'
    VF = 'VF'
    EV = 'EV'
    EE = 'EE'
    EF = 'EF'
    FE = 'FE'
    FV = 'FV'
    FF = 'FF'

    VE_star = 'VE_star'
    FE_star = 'FE_star'
