import os

from config import DATA_FILE_EXTENSION
from constructs.relations import Relations, EV, EF, FE, VE
from data_structures.data_structure import DataStructure
from enums import DataStructureNames, NamedRelations
from log import LOGGER
from utils.general import get_successor, remove_consecutive_duplicates


class Symmetric(DataStructure):
    name = DataStructureNames.Symmetric.value

    data = {
        NamedRelations.EV.value: Relations(),
        NamedRelations.EF.value: Relations(),
        NamedRelations.VE.value: Relations(),
        NamedRelations.FE.value: Relations()
    }

    def __init__(self, data_directory):
        ev_file = os.path.join(data_directory, NamedRelations.EV.value + DATA_FILE_EXTENSION)
        ef_file = os.path.join(data_directory, NamedRelations.EF.value + DATA_FILE_EXTENSION)
        fe_file = os.path.join(data_directory, NamedRelations.FE.value + DATA_FILE_EXTENSION)
        ve_file = os.path.join(data_directory, NamedRelations.VE.value + DATA_FILE_EXTENSION)

        for f in [ev_file, ef_file, fe_file, ve_file]:
            if not os.path.exists(f):
                LOGGER.error(f"File {f} does not exist.")
                raise FileNotFoundError(f"File {f} does not exist.")

        self.data[NamedRelations.EV.value] = EV(ev_file)
        self.data[NamedRelations.EF.value] = EF(ef_file)
        self.data[NamedRelations.FE.value] = FE(fe_file)
        self.data[NamedRelations.VE.value] = VE(ve_file)

    def get_relation(self, relation_name: str, entity: str):
        if relation_name not in NamedRelations._value2member_map_:
            LOGGER.error(f"Relation name {relation_name} is unknown.")
            raise ValueError(f"Relation name {relation_name} is unknown.")

        # stored relations
        if relation_name in [
            NamedRelations.EV.value,
            NamedRelations.EF.value,
            NamedRelations.VE.value,
            NamedRelations.FE.value
        ]:
            LOGGER.debug(f"Retrieving (stored) relation {relation_name}({entity})")
            result = self.data[relation_name].get_relation(entity)
        else:

            # derived relations
            LOGGER.debug(f"Retrieving (derived) relation {relation_name}({entity})")

            if relation_name == NamedRelations.EE.value:
                result = self.__get_EE(entity)
            elif relation_name == NamedRelations.FF.value:
                result = self.__get_FF(entity)
            elif relation_name == NamedRelations.FV.value:
                result = self.__get_FV(entity)
            elif relation_name == NamedRelations.VV.value:
                result = self.__get_VV(entity)
            elif relation_name == NamedRelations.VF.value:
                result = self.__get_VF(entity)
            else:
                raise NotImplementedError(f"{relation_name} has not been implemented for {self.name}")

        LOGGER.debug(f"{relation_name}({entity}) = {result}")
        return result

    def __get_VF(self, vertex):
        result = []

        # edges connected to vertex
        ve_of_vertex = self.get_relation(NamedRelations.VE.value, vertex)
        LOGGER.debug(f"Edges connected to vertex {vertex}: "
                     f"{NamedRelations.VE.value}({vertex}) = {ve_of_vertex}")

        for edge in ve_of_vertex:
            # two endpoints of edge
            two_endpoints_of_edge = self.get_relation(NamedRelations.EV.value, edge)
            LOGGER.debug(f"Two endpoints of edge {edge}: "
                         f"{NamedRelations.EV.value}({vertex} = {two_endpoints_of_edge}")

            # two faces connected to edge
            two_faces_connected_to_edge = self.get_relation(NamedRelations.EF.value, edge)
            LOGGER.debug(f"Two faces connected to edge {edge}: "
                         f"{NamedRelations.EF.value}({edge} "
                         f"= {two_faces_connected_to_edge}")

            if vertex == two_endpoints_of_edge[0]:
                # if vertex is the origin endpoint of edge
                # add the face to the left of edge to result list
                LOGGER.debug(f"Vertex {vertex} is the origin endpoint of edge {edge}")

                face_to_add = two_faces_connected_to_edge[0]
            elif vertex == two_endpoints_of_edge[1]:
                # if vertex is the target endpoint of edge
                # add the face to the right of edge to result list
                face_to_add = two_faces_connected_to_edge[1]
            else:
                LOGGER.error(f"This should never happen.")
                raise Exception(f"This should never happen.")

            result.append(face_to_add)
            LOGGER.debug(f"Adding face {face_to_add} to result list {result}")

        LOGGER.debug(f"Returning result list: {result}")
        return result

    def __get_VV(self, vertex):
        result = []

        # edges connected to vertex
        ve_of_vertex = self.get_relation(NamedRelations.VE.value, vertex)
        LOGGER.debug(f"Edges connected to vertex {vertex}: "
                     f"{NamedRelations.VE.value}({vertex}) = {ve_of_vertex}")

        for edge in ve_of_vertex:
            # two end points connected to edge
            two_endpoints_of_edge = self.get_relation(NamedRelations.EV.value, edge)
            LOGGER.debug(f"Two endpoints of edge {edge}: "
                         f"{NamedRelations.EV.value}({edge}) = {two_endpoints_of_edge}")

            # the endpoint of edge which is not vertex
            the_other_end_point = two_endpoints_of_edge[0] \
                if vertex != two_endpoints_of_edge[0] \
                else two_endpoints_of_edge[1]

            result.append(the_other_end_point)
            LOGGER.debug(f"Adding {the_other_end_point} to result list: {result}")

        LOGGER.debug(f"Returning result list: {result}")
        return result

    def __get_FV(self, face):
        result = []

        # edges connected to face
        fe_of_face = self.get_relation(NamedRelations.FE.value, face)
        LOGGER.debug(f"Edges connected to face {face}: "
                     f"{NamedRelations.FE.value}({face}) = {fe_of_face}")

        for edge in fe_of_face:
            endpoints_of_edge = self.get_relation(NamedRelations.EV.value, edge)
            LOGGER.debug(f"Endpoints of edge {edge}: "
                         f"{NamedRelations.EV.value}(edge) = {endpoints_of_edge}")

            # consider the two faces connected to edge
            ef_of_edge = self.get_relation(NamedRelations.EF.value, edge)
            LOGGER.debug(f"Two faces connected to {edge}: "
                         f"{NamedRelations.EF.value}({edge}) = {ef_of_edge}")

            if face == ef_of_edge[0]:
                # face is on the left of edge
                # add the two endpoints of edge to result list
                LOGGER.debug(f"Face {face} is on the left of edge {edge}")
                result.extend(endpoints_of_edge)
                LOGGER.debug(f"Adding {endpoints_of_edge} to result list: {result}")
            elif face == ef_of_edge[1]:
                # face is on the right of edge
                # reverse the order of the two endpoints and add them to result list
                LOGGER.debug(f"Face {face} is on the right of edge {edge}")
                reversed_endpoints_of_edge = endpoints_of_edge.copy()
                reversed_endpoints_of_edge.reverse()
                result.extend(reversed_endpoints_of_edge)
                LOGGER.debug(f"Adding {reversed_endpoints_of_edge} to result list: {result}")
            else:
                LOGGER.error(f"This should never happen.")
                raise Exception(f"This should never happen.")

        # remove consecutive duplicates
        LOGGER.debug(f"Remove consecutive elements in result list: {result}")
        result = remove_consecutive_duplicates(result)

        LOGGER.debug(f"Returning result list: {result}")
        return result

    def __get_FF(self, face):
        result = []

        # edges connected to face
        fe_of_face = self.get_relation(NamedRelations.FE.value, face)
        LOGGER.debug(f"Edges connected to face {face}: "
                     f"{NamedRelations.FE.value}({face}) = {fe_of_face}")

        for edge in fe_of_face:
            two_faces_of_edge = self.get_relation(NamedRelations.EF.value, edge)
            LOGGER.debug(f"Two faces of edge {edge}: "
                         f"{NamedRelations.EF.value}({edge}) = {two_faces_of_edge}")

            face_to_add = two_faces_of_edge[0] \
                if face != two_faces_of_edge[0] \
                else two_faces_of_edge[1]
            LOGGER.debug(f"Adding face {face_to_add} to result list: {result}")

            result.append(face_to_add)

        LOGGER.debug("Removing consecutive elements in result")
        result = remove_consecutive_duplicates(result)

        LOGGER.debug(f"Returning result list: {result}")
        return result

    def __get_EE(self, edge):
        result = []

        two_endpoints_of_edge = self.get_relation(NamedRelations.EV.value, edge)
        LOGGER.debug(f"Two endpoints of edge {edge}: {NamedRelations.EV.value}({edge}) = {two_endpoints_of_edge}")

        # go through each endpoint in the list in order
        for endpoint in two_endpoints_of_edge:
            # list of edges incident in endpoint
            ve_of_endpoint = self.get_relation(NamedRelations.VE.value, endpoint)
            LOGGER.debug(f"Edges incident in endpoint {endpoint}: {NamedRelations.VE.value}({endpoint}) "
                         f"= {ve_of_endpoint}")

            # successor of edge if ve
            successor_of_edge_in_ve = get_successor(edge, ve_of_endpoint)
            LOGGER.debug(f"Successor of edge {edge}: {successor_of_edge_in_ve}")

            result.append(successor_of_edge_in_ve)
            LOGGER.debug(f"Adding edge {edge} to result list: {result}")

        LOGGER.debug(f"Returning result list: {result}")
        return result
