from enums import NamedRelations
from log import LOGGER


class DataStructure:

    name = 'undefined'
    data = {}

    def get_relation(self, relation_name: str, entity: str) -> list:
        """
        Retrieve relation `relation_name` of entity `entity`.
        :param relation_name: name of a relation (e.g., VV, VE, EE, EF, etc.)
        :param entity: ID of an entity (e.g., e1, f2)
        :return: a list if entities associated with `entity` through relation `relation_name`
        """
        pass

    def print(self):
        for entry in self.data:
            print(f"--- Relations {entry} ---")
            self.data[entry].print()
