import os
from config import DATA_FILE_EXTENSION
from constructs.relations import EV, Relations, DCEL_VE_star, EE, EF, DCEL_FE_star
from data_structures.data_structure import DataStructure
from log import LOGGER
from enums import NamedRelations, DataStructureNames
from utils.general import remove_consecutive_duplicates


class DCEL(DataStructure):
    name = DataStructureNames.DCEL.value

    data = {
        NamedRelations.EV.value: Relations(),
        NamedRelations.EF.value: Relations(),
        NamedRelations.EE.value: Relations(),
        NamedRelations.VE_star.value: Relations(),
        NamedRelations.FE_star.value: Relations()
    }

    def __init__(self, data_directory):
        ev_file = os.path.join(data_directory, NamedRelations.EV.value + DATA_FILE_EXTENSION)
        ef_file = os.path.join(data_directory, NamedRelations.EF.value + DATA_FILE_EXTENSION)
        ee_file = os.path.join(data_directory, NamedRelations.EE.value + DATA_FILE_EXTENSION)
        ve_star_file = os.path.join(data_directory, NamedRelations.VE_star.value + DATA_FILE_EXTENSION)
        fe_star_file = os.path.join(data_directory, NamedRelations.FE_star.value + DATA_FILE_EXTENSION)

        for f in [ev_file, ef_file, ee_file, ve_star_file, fe_star_file]:
            if not os.path.exists(f):
                LOGGER.error(f"File {f} does not exist.")
                raise FileNotFoundError(f"File {f} does not exist.")

        self.data[NamedRelations.EV.value] = EV(ev_file)
        self.data[NamedRelations.EF.value] = EF(ef_file)
        self.data[NamedRelations.EE.value] = EE(ee_file)
        self.data[NamedRelations.VE_star.value] = DCEL_VE_star(ve_star_file)
        self.data[NamedRelations.FE_star.value] = DCEL_FE_star(fe_star_file)

    def get_relation(self, relation_name: str, entity: str):
        if relation_name not in NamedRelations._value2member_map_:
            LOGGER.error(f"Relation name {relation_name} is unknown.")
            raise ValueError(f"Relation name {relation_name} is unknown.")

        # stored relations
        if relation_name in [
            NamedRelations.EV.value,
            NamedRelations.EF.value,
            NamedRelations.EE.value,
            NamedRelations.VE_star.value,
            NamedRelations.FE_star.value
        ]:
            LOGGER.debug(f"Retrieving (stored) relation {relation_name}({entity})")
            result = self.data[relation_name].get_relation(entity)
        else:

            # derived relations
            LOGGER.debug(f"Retrieving (derived) relation {relation_name}({entity})")

            if relation_name == NamedRelations.FE.value:
                result = self.__get_FE(entity)
            elif relation_name == NamedRelations.FF.value:
                result = self.__get_FF(entity)
            elif relation_name == NamedRelations.FV.value:
                result = self.__get_FV(entity)
            elif relation_name == NamedRelations.VE.value:
                result = self.__get_VE(entity)
            elif relation_name == NamedRelations.VV.value:
                result = self.__get_VV(entity)
            elif relation_name == NamedRelations.VF.value:
                result = self.__get_VF(entity)
            else:
                raise NotImplementedError(f"{relation_name} has not been implemented for {self.name}")

        LOGGER.debug(f"{relation_name}({entity}) = {result}")
        return result

    def __get_VF(self, vertex):
        result = []

        ve_of_vertex = self.__get_VE(vertex)
        LOGGER.debug(f"Edges connected to vertex {vertex}: "
                     f"{NamedRelations.VE.value}({vertex}) = {ve_of_vertex}")

        for edge in ve_of_vertex:
            # two endpoints of edge
            two_endpoints_of_edge = self.get_relation(NamedRelations.EV.value, edge)
            LOGGER.debug(f"Two endpoints of edge {edge}: "
                         f"{NamedRelations.EV.value}({vertex} = {two_endpoints_of_edge}")

            # two faces connected to edge
            two_faces_connected_to_edge = self.get_relation(NamedRelations.EF.value, edge)
            LOGGER.debug(f"Two faces connected to edge {edge}: "
                         f"{NamedRelations.EF.value}({edge} = {two_faces_connected_to_edge}")

            if vertex == two_endpoints_of_edge[0]:
                # if vertex is the origin endpoint of edge
                # add the face to the left of edge to result list
                LOGGER.debug(f"Vertex {vertex} is the origin endpoint of edge {edge}")

                face_to_add = two_faces_connected_to_edge[0]
            elif vertex == two_endpoints_of_edge[1]:
                # if vertex is the target endpoint of edge
                # add the face to the right of edge to result list
                face_to_add = two_faces_connected_to_edge[1]
            else:
                raise Exception(f"This should never happen.")

            result.append(face_to_add)
            LOGGER.debug(f"Adding face {face_to_add} to result list {result}")

        LOGGER.debug(f"Returning result list: {result}")
        return result

    def __get_VV(self, vertex):
        result = []
        ve_of_vertex = self.__get_VE(vertex)
        LOGGER.debug(f"Edges connected to vertex {vertex}: "
                     f"{NamedRelations.VE.value}({vertex}) = {ve_of_vertex}")

        for edge in ve_of_vertex:
            two_vertices_connected_to_edge = self.get_relation(NamedRelations.EV.value, edge)
            LOGGER.debug(f"Two vertices connected to edge {edge}: "
                         f"{NamedRelations.EV.value}({edge}) = {two_vertices_connected_to_edge}")

            if vertex == two_vertices_connected_to_edge[0]:
                # if vertex is the origin endpoint of edge
                LOGGER.debug(f"Vertex {vertex} is the origin endpoint of edge {edge}")
                result.append(two_vertices_connected_to_edge[1])
                LOGGER.debug(f"Adding vertex {two_vertices_connected_to_edge[1]} to result list: {result}")
            else:
                # if vertex is the target endpoint of edge
                LOGGER.debug(f"Vertex {vertex} is the target endpoint of edge {edge}")
                result.append(two_vertices_connected_to_edge[0])
                LOGGER.debug(f"Adding vertex {two_vertices_connected_to_edge[0]} to result list: {result}")

        LOGGER.debug(f"Removing consecutive elements from result list: {result}")
        result = remove_consecutive_duplicates(result)

        LOGGER.debug(f"Returning result list: {result}")
        return result

    def __get_VE(self, vertex):
        result = []

        # the first edge stored in VE*
        edge = self.get_relation(NamedRelations.VE_star.value, vertex)[0]
        LOGGER.debug(f"First edge connected to vertex {vertex}: "
                     f"{NamedRelations.VE_star.value}({vertex}) = {edge}")
        result.append(edge)

        e_init = edge
        LOGGER.debug(f"Set initial edge e_init = {e_init}")

        idx = 0
        while edge != e_init or idx == 0:
            # two endpoints of e
            two_endpoints_of_edge = self.get_relation(NamedRelations.EV.value, edge)
            LOGGER.debug(f"Two endpoints of edge {edge}: "
                         f"{NamedRelations.EV.value}({edge}) = {two_endpoints_of_edge}")

            if vertex == two_endpoints_of_edge[0]:
                # if vertex is the origin endpoint of e
                # the next edge is the fist edge in EE(e)
                LOGGER.debug(f"Vertex {vertex} is the origin endpoint of {edge}")
                edge = self.get_relation(NamedRelations.EE.value, edge)[0]

            elif vertex == two_endpoints_of_edge[1]:  # if vertex is the target endpoint of e
                LOGGER.debug(f"Vertex {vertex} is the target endpoint of {edge}")
                edge = self.get_relation(NamedRelations.EE.value, edge)[1]
            else:
                LOGGER.error(f"This should never happen.")
                raise ValueError(f"This should never happen.")

            if edge != e_init:
                result.append(edge)
                LOGGER.debug(f"Adding edge {edge} to result list: {result}")

            idx += 1

        LOGGER.debug(f"Returning result list: {result}")
        return result

    def __get_FV(self, face: str) -> list:
        result = []

        fe_of_face = self.__get_FE(face)
        LOGGER.debug(f"Edges on the boundary of face {face}: "
                     f"{NamedRelations.FE.value}({face}) = {fe_of_face}")

        for edge in fe_of_face:
            two_vertices_connected_to_edge = self.get_relation(NamedRelations.EV.value, edge)
            LOGGER.debug(f"Two vertices connected to edge {edge}: "
                         f"{NamedRelations.EV.value}({edge}) = {two_vertices_connected_to_edge}")

            if face == self.get_relation(NamedRelations.EF.value, edge)[0]:
                # if f is on the left of edge
                LOGGER.debug(f"Face {face} is on the left of edge {edge}")
                result.extend(two_vertices_connected_to_edge)
                LOGGER.debug(f"Adding {two_vertices_connected_to_edge} to result list: {result}")
            else:
                # if f is on the right of edge
                LOGGER.debug(f"Face {face} is on the right of edge {edge}")
                reversed_vertex_list = two_vertices_connected_to_edge.copy()
                reversed_vertex_list.reverse()
                result.extend(reversed_vertex_list)
                LOGGER.debug(f"Adding {reversed_vertex_list} to result list: {result}")

        LOGGER.debug(f"Remove consecutive elements from result list: {result}")
        result = remove_consecutive_duplicates(result)

        LOGGER.debug(f"Returning result list: {result}")
        return result

    def __get_FF(self, face: str) -> list:
        result = []

        fe_of_face = self.__get_FE(face)
        LOGGER.debug(f"Edges on the boundary of face {face}: "
                     f"{NamedRelations.FE.value}({face}) = {fe_of_face}")

        for edge in fe_of_face:
            two_faces_connected_to_edge = self.get_relation(NamedRelations.EF.value, edge)
            LOGGER.debug(f"Two faces connected to edge {edge}: "
                         f"{NamedRelations.EF.value}({edge}) = {two_faces_connected_to_edge}")

            face_to_add = two_faces_connected_to_edge[0] \
                if face != two_faces_connected_to_edge[0] \
                else two_faces_connected_to_edge[1]

            result.append(face_to_add)
            LOGGER.debug(f"Adding face {face_to_add} to result list {result}")

        LOGGER.debug(f"Remove consecutive duplicates from result list: {result}")
        result = remove_consecutive_duplicates(result)

        LOGGER.debug(f"Returning result list: {result}")
        return result

    def __get_FE(self, face: str) -> list:
        result = []

        # first bounding edge <- the edge stored in FE*
        edge = self.get_relation(NamedRelations.FE_star.value, face)[0]
        LOGGER.debug(f"First edge connected to face {face}: "
                     f"{NamedRelations.FE_star.value}({face}) = {edge}")

        # initial edge
        e_init = edge
        LOGGER.debug(f"Set initial edge e_init = {e_init}")

        last_known_vertex = ''

        idx = 0
        while idx == 0 or edge != e_init:
            # if the given face is on the left hand side of edge e

            ef_of_edge = self.get_relation(NamedRelations.EF.value, edge)
            left_face_of_edge = ef_of_edge[0]
            right_face_of_edge = ef_of_edge[1]

            if face == left_face_of_edge:
                LOGGER.debug(f"Face {face} is on the left of edge {edge}")
                result.append(edge)
                LOGGER.debug(f"Adding edge {edge} to the result list: {result}")

                # last known vertex on the boundary of face
                last_known_vertex = self.get_relation(NamedRelations.EV.value, edge)[1]
                LOGGER.debug(f"Last known vertex: "
                             f"{NamedRelations.EV.value}({edge}) = {last_known_vertex}")

                # successor edge
                edge = self.get_relation(NamedRelations.EE.value, edge)[1]
                LOGGER.debug(f"Successor edge: "
                             f"{NamedRelations.EE.value}({edge})[1] = {edge}")
            elif face == right_face_of_edge:
                LOGGER.debug(f"Face {face} is on the right of edge {edge}")
                result.append(edge)
                LOGGER.debug(f"Adding {edge} to the result list {result}")

                # last known point on the boundary of face
                last_known_vertex = self.get_relation(NamedRelations.EV.value, edge)[0]
                LOGGER.debug(f"Last known vertex: "
                             f"{NamedRelations.EV.value}({edge}) = {last_known_vertex}")

                # successor edge
                edge = self.get_relation(NamedRelations.EE.value, edge)[0]
                LOGGER.debug(f"Successor edge: "
                             f"{NamedRelations.EE.value}({edge})[0] = {edge}")
            else:
                LOGGER.debug(f"Face {face} does not have edge {edge} on its boundary. "
                             f"Start pivoting around last known vertex {last_known_vertex}.")

                #left_face_of_edge = self.get_relation(NamedRelations.EF.value, edge)[0]
                #LOGGER.debug(f"Left face of {edge}: "
                #             f"{NamedRelations.EF.value}({edge})[0] = {left_face_of_edge}")

                #right_face = self.get_relation(NamedRelations.EF.value, edge)[1]
                #LOGGER.debug(f"Right face of {edge}: "
                #             f"{NamedRelations.EF.value}({edge})[1] = {right_face}")

                while left_face_of_edge != face and right_face_of_edge != face:
                    ev_of_edge = self.get_relation(NamedRelations.EV.value, edge)
                    LOGGER.debug(f"Endpoints of edge {edge}: "
                                 f"{NamedRelations.EV.value}({edge}) = {ev_of_edge}")

                    ee_of_edge = self.get_relation(NamedRelations.EE.value, edge)
                    LOGGER.debug(f"Edges connected to edge {edge}: "
                                 f"{NamedRelations.EE.value}({edge}) = {ee_of_edge}")

                    if last_known_vertex == ev_of_edge[0]:
                        LOGGER.debug(f"Last known vertex {last_known_vertex} is the origin end point of edge {edge}")

                        edge = ee_of_edge[0]
                        LOGGER.debug(f"Considering successor edge: "
                                     f"{NamedRelations.EE.value}({edge})[0] = {edge}")
                    elif last_known_vertex == ev_of_edge[1]:
                        LOGGER.debug(f"Last known vertex {last_known_vertex} is the target end point of edge {edge}")

                        edge = ee_of_edge[1]
                        LOGGER.debug(f"Considering successor edge: "
                                     f"{NamedRelations.EE.value}({edge})[1] = {edge}")
                    else:
                        LOGGER.error(f"This should never happen.")
                        raise Exception(f"This should never happen.")

                    ef_of_edge = self.get_relation(NamedRelations.EF.value, edge)

                    left_face_of_edge = ef_of_edge[0]
                    LOGGER.debug(f"Left face of {edge}: "
                                 f"{NamedRelations.EF.value}({edge})[0] = {left_face_of_edge}")

                    right_face_of_edge = ef_of_edge[1]
                    LOGGER.debug(f"Right face of {edge}: "
                                 f"{NamedRelations.EF.value}({edge})[1] = {right_face_of_edge}")

            idx += 1

        LOGGER.debug(f"Edge loop closed, {result} - {edge}")
        LOGGER.debug(f"Returning result list: {result}")
        return result
