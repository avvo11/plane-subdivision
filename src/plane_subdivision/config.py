# Data file format
DELIMITER = ','
COMMENT = '#'
DATA_FILE_EXTENSION = '.txt'

# Logging
LOG_FORMAT = "%(asctime)s %(levelname)-7s %(filename)10s:%(lineno)-3d - %(message)s"
LOG_FILE_NAME = "%Y%m%d-%H%M%S.log"
LOG_DIR = "logs/"

PROMPT_SYMBOL = '>>>'
