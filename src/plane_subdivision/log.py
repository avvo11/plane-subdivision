import logging
from datetime import datetime
import os

from config import LOG_FORMAT, LOG_DIR, LOG_FILE_NAME

LOGGER = logging.getLogger("Plane Subdivision")
LOGGER.setLevel(level=logging.DEBUG)

formatter = logging.Formatter(LOG_FORMAT)

# Log to files
os.makedirs(LOG_DIR, exist_ok=True)
fh = logging.FileHandler(os.path.join(LOG_DIR,
                                      str(datetime.now().strftime(LOG_FILE_NAME))),
                         mode='w', encoding='utf-8')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
LOGGER.addHandler(fh)

# Log to screen
ch = logging.StreamHandler()
ch.setFormatter(formatter)
ch.setLevel(logging.INFO)
LOGGER.addHandler(ch)
