import argparse
import os

from config import DATA_FILE_EXTENSION, PROMPT_SYMBOL
from constructs.relations import EV
from data_structures.data_structure import DataStructure
from data_structures.dcel import DCEL
from data_structures.symmetric import Symmetric
from enums import DataStructureNames

from log import LOGGER


def parse_opt():
    parser = argparse.ArgumentParser()

    parser.add_argument('--data_structure_name', type=str,
                        choices=[n.value for n in DataStructureNames],
                        required=True, help='name of data structure')
    parser.add_argument('--data_directory', type=str, required=True,
                        help='path to directory of data files to be loaded')
    opt = parser.parse_args()
    return opt


def main(opt):
    structure: DataStructure

    try:
        if opt.data_structure_name == DataStructureNames.DCEL.value:
            structure = DCEL(opt.data_directory)
        elif opt.data_structure_name == DataStructureNames.Symmetric.value:
            structure = Symmetric(opt.data_directory)
        else:
            LOGGER.error(f"Unrecognised data structure name: {opt.name_of_data_structure}")
            raise Exception(f"Unrecognised data structure name: {opt.name_of_data_structure}")
    except:
        LOGGER.error(f"Could not load data structure {opt.data_structure_name} from {opt.data_directory}). "
                     f"Please check the input parameters and data and try again.")
        return
    else:
        LOGGER.info(f"Data structure {opt.data_structure_name} "
                    f"successfully loaded from {opt.data_directory}")

    while True:
        query = input(f"Submit a query [e.g., EE(e1)] or type `q` to exit\n{PROMPT_SYMBOL} ")
        query = query.replace(" ", "")

        if query.lower() == 'q':
            LOGGER.debug(f"Exiting program")
            break

        relation = query[:query.find('(')]
        entity = query[query.find('(') + 1:query.find(')')]

        LOGGER.debug(f"===== New query received {relation}({entity}) =====")

        try:
            result = structure.get_relation(relation, entity)
        except:
            LOGGER.error(f"Failed to process query {relation}({entity})")
            continue
        else:
            print(f"{PROMPT_SYMBOL} {relation}({entity}) = {result}")


if __name__ == "__main__":
    opt = parse_opt()
    main(opt)
