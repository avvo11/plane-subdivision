from enums import NamedRelations
from utils.file_utils import parse


class Relations:
    """A set of relations between pairs of entities of types V, E, F in a plane subdivision."""
    # type of relation (e.g., EV, EF, etc.) that is overridden by subclasses
    relation_type = 'undefined'

    # this is where relations are stored
    # each key in the dict is a spatial entity (e.g., e1)
    # the value corresponding to each key is a list of entities associated with entity
    # in question through relation of type `type` (e.g., [e2, e5])
    data = {}

    def __init__(self, file=''):
        """
        Load relation from file `file`.
        :param file: path to the file that stores relation information
        """
        if file != '':
            self.data = parse(file)

    def print(self):
        """
        Print relation to screen.
        """
        for key, value in self.data.items():
            print(f'{self.relation_type}({key}) = {value}')

    def get_relation(self, entity) -> list:
        """
        Get relation of given `entity`.
        :param entity: the entity in question
        :return: a list of entities associated with `entity` (the entity in question)
        """
        # TODO: catch entity does not exist
        return self.data[entity]


class EV(Relations):
    relation_type = NamedRelations.EV.value


class EE(Relations):
    relation_type = NamedRelations.EE.value


class EF(Relations):
    relation_type = NamedRelations.EF.value


class DCEL_FE_star(Relations):
    relation_type = NamedRelations.FE_star.value


class DCEL_VE_star(Relations):
    relation_type = NamedRelations.VE_star.value


class FE(Relations):
    relation_type = NamedRelations.FE.value


class VE(Relations):
    relation_type = NamedRelations.VE.value
